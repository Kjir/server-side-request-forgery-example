require 'sinatra'
require 'open-uri'

set :bind, '0.0.0.0'

get '/' do
  format 'Response: %s', open(params[:url]).read
end
